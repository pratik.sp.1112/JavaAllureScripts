package alluresitesscript;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class ElementsFinder {

    public static WebElement findByMultipleAttributes(WebDriver driver, String tag, String[] attributes,
            String[] values) {
        List<WebElement> htmlFields = null;

        if (attributes.length == values.length && tag != null && driver != null) {

            if (ifElementExists(driver, tag)) {
                htmlFields = driver.findElements(By.tagName(tag));
            } else {
                return null;
            }

            loop1:
            for (int i = 0; i < htmlFields.size(); i++) {
                for (int attrs = 0; attrs < attributes.length; attrs++) {
                    if (!htmlFields.get(i).getAttribute(attributes[attrs]).equals(values[attrs])) {
                        continue loop1;
                    }
                    return htmlFields.get(i);
                }
            }
        }

        return null;
    }

    public static WebElement findByMultipleParameters(WebDriver driver, String tag, String attribute, String value,
            String text) {
        List<WebElement> htmlFields = null;

        if (tag != null && attribute != null && value != null && text == null) {
            if (ifElementExists(driver, tag)) {
                htmlFields = driver.findElements(By.tagName(tag));
            } else {
                return null;
            }
//			System.out.println(htmlFields.size());

            for (int i = 0; i < htmlFields.size(); i++) {
//				System.out.println(htmlFields.get(i).getAttribute(attribute));

                if (htmlFields.get(i).getAttribute(attribute).equals(value)) {
                    return htmlFields.get(i);
                }
            }

        } else if (tag != null && text != null) {
            if (ifElementExists(driver, tag)) {
                htmlFields = driver.findElements(By.tagName(tag));
            } else {
                return null;
            }

            for (int i = 0; i < htmlFields.size(); i++) {
                if (htmlFields.get(i).getText().equals(text)) {
                    return htmlFields.get(i);
                }
            }

        } else if (tag != null && attribute != null && value != null && text != null) {
            if (ifElementExists(driver, tag)) {
                htmlFields = driver.findElements(By.tagName(tag));
            } else {
                return null;
            }

            for (int i = 0; i < htmlFields.size(); i++) {
                if (htmlFields.get(i).getAttribute(attribute).equals(value)) {
                    if (htmlFields.get(i).getText().equals(text)) {
                        return htmlFields.get(i);
                    }
                }
            }

        }

        return null;
    }

    public static WebElement findByMultipleParameters(WebDriver driver, String tag, String attribute, String value,
            boolean ExactEqual, String text) {
        List<WebElement> htmlFields = null;

        if (tag != null && attribute != null && value != null && text == null) {
//            System.out.println(tag);
//            System.out.println(attribute);
//            System.out.println(value);
//            System.out.println(ExactEqual);
//            System.out.println(text);
//            System.out.println("---------");

            if (ifElementExists(driver, tag)) {
                htmlFields = driver.findElements(By.tagName(tag));
            } else {
                return null;
            }

//            System.out.println("Size--" + htmlFields.size());
            for (int i = 0; i < htmlFields.size(); i++) {
//				System.out.println(htmlFields.get(i).getAttribute(attribute));
                if (ExactEqual) {
                    if (htmlFields.get(i).getAttribute(attribute).equals(value)) {
                        return htmlFields.get(i);
                    }
                } else {
                    if (htmlFields.get(i).getAttribute(attribute).contains(value)) {
                        return htmlFields.get(i);
                    }
                }
            }

        } else if (tag != null && text != null) {
            if (ifElementExists(driver, tag)) {
                htmlFields = driver.findElements(By.tagName(tag));
            } else {
                return null;
            }

            for (int i = 0; i < htmlFields.size(); i++) {
                if (htmlFields.get(i).getText().equals(text)) {
                    return htmlFields.get(i);
                }
            }

        } else if (tag != null && attribute != null && value != null && text != null) {
            if (ifElementExists(driver, tag)) {
                htmlFields = driver.findElements(By.tagName(tag));
            } else {
                return null;
            }

            for (int i = 0; i < htmlFields.size(); i++) {
                if (ExactEqual) {
                    if (htmlFields.get(i).getAttribute(attribute).equals(value)) {
                        return htmlFields.get(i);
                    }
                } else {
                    if (htmlFields.get(i).getAttribute(attribute).contains(value)) {
                        return htmlFields.get(i);
                    }
                }
            }
        }

        return null;
    }

    public static int findIndexByMultipleParameters(WebDriver driver, String tag, String attribute, String value,
            String text) {
        List<WebElement> htmlFields = null;

        if (tag != null && attribute != null && value != null && text == null) {
            if (ifElementExists(driver, tag)) {
                htmlFields = driver.findElements(By.tagName(tag));
            } else {
                return -1;
            }
//			System.out.println(htmlFields.size());

            for (int i = 0; i < htmlFields.size(); i++) {
//				System.out.println(htmlFields.get(i).getAttribute(attribute));

                if (htmlFields.get(i).getAttribute(attribute).equals(value)) {
                    return (i);
                }
            }

        } else if (tag != null && text != null) {
            if (ifElementExists(driver, tag)) {
                htmlFields = driver.findElements(By.tagName(tag));
            } else {
                return -1;
            }

            for (int i = 0; i < htmlFields.size(); i++) {
                if (htmlFields.get(i).getText().equals(text)) {
                    return (i);
                }
            }

        } else if (tag != null && attribute != null && value != null && text != null) {
            if (ifElementExists(driver, tag)) {
                htmlFields = driver.findElements(By.tagName(tag));
            } else {
                return -1;
            }

            for (int i = 0; i < htmlFields.size(); i++) {
                if (htmlFields.get(i).getAttribute(attribute).equals(value)) {
                    if (htmlFields.get(i).getText().equals(text)) {
                        return (i);
                    }
                }
            }

        }

        return -1;
    }

    public static int findIndexByMultipleParameters(WebDriver driver, String tag, String attribute, String value,
            boolean ExactEqual, String text) {
        List<WebElement> htmlFields = null;

        if (tag != null && attribute != null && value != null && text == null) {
            if (ifElementExists(driver, tag)) {
                htmlFields = driver.findElements(By.tagName(tag));
            } else {
                return -1;
            }
//			System.out.println(htmlFields.size());

            for (int i = 0; i < htmlFields.size(); i++) {
//				System.out.println(htmlFields.get(i).getAttribute(attribute));

                if (ExactEqual) {
                    if (htmlFields.get(i).getAttribute(attribute).equals(value)) {
                        return (i);
                    }
                } else {
                    if (htmlFields.get(i).getAttribute(attribute).contains(value)) {
                        return (i);
                    }
                }
            }

        } else if (tag != null && text != null) {
            if (ifElementExists(driver, tag)) {
                htmlFields = driver.findElements(By.tagName(tag));
            } else {
                return -1;
            }

            for (int i = 0; i < htmlFields.size(); i++) {
                if (htmlFields.get(i).getText().equals(text)) {
                    return (i);
                }
            }

        } else if (tag != null && attribute != null && value != null && text != null) {
            if (ifElementExists(driver, tag)) {
                htmlFields = driver.findElements(By.tagName(tag));
            } else {
                return -1;
            }

            for (int i = 0; i < htmlFields.size(); i++) {
                if (htmlFields.get(i).getAttribute(attribute).equals(value)) {

                    if (ExactEqual) {
                        if (htmlFields.get(i).getAttribute(attribute).equals(value)) {
                            return (i);
                        }
                    } else {
                        if (htmlFields.get(i).getAttribute(attribute).contains(value)) {
                            return (i);
                        }
                    }
                }
            }

        }

        return -1;
    }

    public static boolean ifElementExists(WebDriver driver, String tag) {
        driver.manage().timeouts().implicitlyWait(0, TimeUnit.MILLISECONDS);
        boolean exists = driver.findElements(By.tagName(tag)).size() != 0;
        driver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);

//        if (driver.findElement(By.tagName(tag)) == null) {
//            return false;
//        }
//        return true;
        return exists;
    }

}
