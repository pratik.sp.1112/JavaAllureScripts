package alluresitesscript;

import java.util.List;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;
//import org.testng.annotations.Test;
//import org.testng.annotations.BeforeTest;
//import org.testng.annotations.AfterTest;

public class FormType2 {

	public static WebDriver driver = null;
	// All Id attribute
	public static String FullName = "name";
	public static String Email = "email";
	public static String Country = "drpCountry";
	public static String City = "drpCity";
	public static String Mobile = "tel";
	public static String Procedure = "racbSelectedProcedure_Input";
	public static String MessageField = "txtQuerry";
	public static String CaptchaSpan = "txtCaptcha";
	public static String CaptchaField = "captchat"; // name attribute
	public static String SubmitBtn = "btnSubmit";
        public static TestData td;

	public void startSingleTest() {
		WebElement frame = ElementsFinder.findByMultipleParameters(driver, "iframe", "src",
				"http://52.207.23.10/enquiry/commonenquiry.php",false, null);
		WebElement captchaField = null;
		WebElement captchaSpan = null;
		Select CountrySelector = null;
		Select CitySelector = null;

		driver.switchTo().frame(frame);

		captchaField = driver.findElement(By.name(CaptchaField));


		driver.findElement(By.id(FullName)).sendKeys(td.FullName);
		driver.findElement(By.id(Email)).sendKeys(td.Email);
		driver.findElement(By.id(Mobile)).sendKeys(td.Mobile);
		driver.findElement(By.id(Procedure)).sendKeys(td.Procedure);
		driver.findElement(By.id(MessageField)).sendKeys(td.MessageField);

		CountrySelector = new Select(driver.findElement(By.id(Country)));
		CountrySelector.selectByVisibleText(td.CountryName);

		CitySelector = new Select(driver.findElement(By.id(City)));
		td.waitTillListPopulates(CitySelector);
		CitySelector.selectByVisibleText(td.CityName);

		captchaField = driver.findElement(By.name(CaptchaField));
		captchaSpan = driver.findElement(By.id(CaptchaSpan));
//		System.out.println(captchaSpan.getText());
//		System.out.println(captchaSpan.getAttribute("id"));
		// System.out.println(captchaSpan.get());
		// captchaField.sendKeys(td.getCaptchaText(captchaSpan));
		captchaField.sendKeys(captchaSpan.getText().replaceAll(" ", ""));

		driver.findElement(By.id(SubmitBtn)).click();
	}

}
