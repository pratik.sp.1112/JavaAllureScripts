package alluresitesscript;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;

public class URLCollection {

    public static Vector<URLandCAT> URLs = null;
    public static Vector<URLandCAT> FixedURLs = null;
    public static Vector<String[]> url_cats = null;
    public static int pointer = 0;
    public static URLandCAT url_cat = null;

//    
//    URLCollection(){
//        try{
//            setURLS();
//        }
//        catch(Exception ex){
//            
//        }
//        
//    }
//    public static String[] url_cat1;// = {
//			"http://www.liposuction-india.com/procedure/non-surgical-techniques/cellulaze.html",
//			"http://www.liposuction-india.com/liposuction-cost.html"
//	};
//    public static String[] url_cat2;// = {
//			"http://www.dimplecreationindia.com/enquiry.html",
//			"http://www.skintreatment-india.com/skin-treatments/laser-hair-removal.html",
//			"http://www.alluremedspa.in/free-download-brochure-form.html",
//			"http://www.drmilandoshi.com/contact-us.html/",
//			"http://www.drmilandoshi.com/cost.html/",
//			"http://www.dimplecreationindia.com/dimple-creation-surgery.html"
//	};
//    public static String[] url_cat3;// = {
//			"http://www.alluremedspa.in/free-download-brochure-form.html",
//			"http://www.alluremedspa.in/breast-augmentation-cost.html",
//			"http://www.alluremedspa.in/contact-us.html",
//			"http://www.alluremedspa.in/cosmetic-surgery/weightloss-surgery.html"
//	};
//    public static String[] url_cat4;// = {
//			"http://www.alluremedspa.in/book-an-appointment.html"
//	};
//    public static String[] url_cat5;// = {
//			"http://www.tummytuck-india.com/cost.html",	
//			"http://www.weightlosssurgery-india.com/procedure/intragastric-gastric-balloon/cost.html",
//			"http://www.rhinoplasty-india.com/cost.html",
//			"http://www.gynecomastia-india.com/gynecomastia-cost.html",
//			"http://www.mommymakeoverindia.com/cost.html",
//			"http://www.weightlosssurgery-india.com/weight-loss-surgery-cost.html",
//			"http://www.alluremedspa.in/breast-augmentation-cost.html",	
//			"http://www.drmilandoshi.com/cost.html/"
//	};
    public static void setURLS(String path) {
        FileReader fin = null;
        try {
            if (path == null) {
                fin = new FileReader(System.getProperty("user.dir") + "\\dataFiles\\URLList_aritro123.txt");
            } else {
                fin = new FileReader(path);
            }
        } catch (FileNotFoundException ex) {
            Logger.getLogger(URLCollection.class.getName()).log(Level.SEVERE, null, ex);
        }

        BufferedReader buf = null;
        try {
            buf = new BufferedReader(fin);

        } catch (Exception ex) {
            Logger.getLogger(URLCollection.class.getName()).log(Level.SEVERE, null, ex);
        }
        String line;

        String url;
        int id;
        FixedURLs = new Vector<URLandCAT>();

        try {
            while ((line = buf.readLine()) != null) {
                if (line.trim().length() <= 0) {
                    continue;
                }
                id = Integer.parseInt(line.substring(0, line.indexOf('}')));
                url = line.substring(line.indexOf('}') + 1).trim();
                FixedURLs.add(new URLandCAT(url, id));
            }
        } catch (IOException ex) {
            Logger.getLogger(URLCollection.class.getName()).log(Level.SEVERE, null, ex);
        }

        try {
            buf.close();
            fin.close();
        } catch (IOException ex) {
            Logger.getLogger(URLCollection.class.getName()).log(Level.SEVERE, null, ex);
        }

        for (URLandCAT t : FixedURLs) {
//            System.out.println(t.category + " - " + t.url);
        }
    }

    public static void initializeURLDataSet() {
//        setURLS();
        URLs = FixedURLs;//new Vector<URLandCAT>();

//        for (int i = 0; i < url_cats.size(); i++) {
////            System.out.println(url_cats.elementAt(i).length);
//            for (String url : url_cats.elementAt(i)) {
//                URLs.add(new URLandCAT(url, i + 1));
//            }
//        }
//
//        for (String url : url_cat1) {
//            URLs.add(new URLandCAT(url, 1));
//        }
//        for (String url : url_cat2) {
//            URLs.add(new URLandCAT(url, 2));
//        }
//        for (String url : url_cat3) {
//            URLs.add(new URLandCAT(url, 3));
//        }
//        for (String url : url_cat4) {
//            URLs.add(new URLandCAT(url, 4));
//        }
//        for (String url : url_cat5) {
//            URLs.add(new URLandCAT(url, 5));
//        }
        pointer = -1;
    }

    public static boolean next() {
        if (URLs.size() < 1) {
            return false;
        } else {
            pointer++;
            if (pointer < URLs.size()) {
                url_cat = URLs.elementAt(pointer);
                return true;
            } else {
                return false;
            }
        }
    }

    public static String getURL() {
        return url_cat.url;
    }

    public static int getCategory() {
        return url_cat.category;
    }

}
