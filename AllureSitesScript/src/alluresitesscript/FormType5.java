package alluresitesscript;

//import org.testng.annotations.Test;
//import org.testng.annotations.BeforeTest;
import java.util.List;
import org.openqa.selenium.By;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
//import org.testng.annotations.AfterTest;

public class FormType5 {

    public static WebDriver driver = null;
    // All name attribute
    public static String FullName = "name1";// id
    public static String Email = "email1";// id
    public static String Age = "age";// id
    public static String Gender = "gender"; // name
    public static String Height = "height";// id
    public static String Weight = "weight";// id
    public static String Procedure = "procedure";// id
    public static String Country = "drpCountry1"; // id
    public static String CityName = "drpCity1";// id
    public static String Mobile = "tel1";// id
    public static String MessageField = "txtQuerry";// id
    public static String Diabetes = "diabetes";// name
    public static String BloodPressure = "bp";// name
    public static String Allergy = "allergy";// name
    public static String SleepApnea = "sleepapnea";// name
    public static String WhatsappCheck = "WhatsApp";// value
    public static String ViberCheck = "Viber";// value
    public static String SkypeCheck = "Skype";// value
    public static String PhoneCheck = "Phone";// value
    public static String MorningCheck = "Morning";// value
    public static String AfternoonCheck = "Afternoon";// value
    public static String EveningCheck = "Evening";// value
    public static String AnytimeCheck = "Anytime";// value
    public static String OtherEstimatedTimeline = "contact_time_other";// id
    public static String Path1 = "uploaded_pic1";// name
    public static String Path2 = "uploaded_pic2";// name
    public static String Path3 = "uploaded_pic3";// name
    public static String captcha = "txtCaptcha";// id
    public static String captchaf = "captchat";// id
    public static String SubmitBtn = "btnSubmit"; // id
    public static TestData td;

    public static void selectRadioButton(String name, String attribute, String value) {
        List<WebElement> element = driver.findElements(By.name(name));
        for (int i = 0; i < element.size(); i++) {
            if (element.get(i).getAttribute(attribute).equals(value)) {
//				System.out.println("found");
                element.get(i).click();
            }
        }
    }

    public boolean isAlertPresent() {
        boolean foundAlert = false;
        WebDriverWait wait = new WebDriverWait(driver, 100 /* timeout in seconds */);
        try {
            wait.until(ExpectedConditions.alertIsPresent());
            foundAlert = true;
        } catch (TimeoutException eTO) {
            foundAlert = false;
        }
        return foundAlert;
    }

    public void startSingleTest() {

//        System.out.println("Testing Started"); 
        WebElement frame = ElementsFinder.findByMultipleParameters(driver, "iframe", "src",
                "http://52.207.23.10/enquiry/commonenquiry-cost.php", false, null);
        driver.switchTo().frame(frame);

        driver.findElement(By.id(FullName)).sendKeys(td.FullName);
        driver.findElement(By.id(Email)).sendKeys(td.Email);
        driver.findElement(By.id(Age)).sendKeys(td.Age);
        driver.findElement(By.name(Gender)).sendKeys(td.Gender);
        driver.findElement(By.id(Height)).sendKeys(td.Height);
        driver.findElement(By.id(Weight)).sendKeys(td.Weight);
        driver.findElement(By.id(Procedure)).sendKeys(td.Procedure);

        Select country = new Select(driver.findElement(By.id(Country)));
        country.selectByVisibleText(td.CountryName);

        Select city = new Select(driver.findElement(By.id(CityName)));
        td.waitTillListPopulates(city);
        city.selectByVisibleText(td.CityName);

        driver.findElement(By.id(Mobile)).sendKeys(td.Mobile);
        driver.findElement(By.id(MessageField)).sendKeys(td.MessageField);

//		System.out.println(td.Diabetes);
        selectRadioButton(Diabetes, "value", td.Diabetes);
        selectRadioButton(BloodPressure, "value", td.BloodPressure);
        selectRadioButton(Allergy, "value", td.Allergy);
        selectRadioButton(SleepApnea, "value", td.SleepApnea);

        ElementsFinder.findByMultipleParameters(driver, "input", "value", WhatsappCheck, null).click();
        ElementsFinder.findByMultipleParameters(driver, "input", "value", MorningCheck, null).click();
        driver.findElement(By.id(OtherEstimatedTimeline)).sendKeys(td.OtherEstimatedTimeline);

        if (td.Path1 != null) {
            if (!td.Path1.equals("")) {
                driver.findElement(By.name(Path1)).sendKeys(td.Path1);
            }
        }
        if (isAlertPresent()) {
            driver.switchTo().alert().accept();
        }

        if (td.Path2 != null) {
            if (!td.Path2.equals("")) {
                driver.findElement(By.name(Path2)).sendKeys(td.Path2);
            }
        }
        if (isAlertPresent()) {
            driver.switchTo().alert().accept();
        }

        if (td.Path3 != null) {
            if (!td.Path3.equals("")) {
                driver.findElement(By.name(Path3)).sendKeys(td.Path3);
            }
        }
        if (isAlertPresent()) {
            driver.switchTo().alert().accept();
        }

        String captchaText = td.getCaptchaText(driver.findElement(By.id(captcha)));
        driver.findElement(By.id(captchaf)).sendKeys(captchaText);

        ElementsFinder.findByMultipleParameters(driver, "input", "id", SubmitBtn, null).click();
    }

}
