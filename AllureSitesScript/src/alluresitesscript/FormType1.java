package alluresitesscript;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;
//import org.testng.annotations.Test;
//import org.testng.annotations.BeforeTest;
//import org.testng.annotations.AfterTest;

public class FormType1 {
	public static WebDriver driver = null;
	public static String FirstName = "FName";
	public static String LastName = "LName";
	public static String Email = "email-656";
	public static String Mobile = "intl_tel-395-cf7it-national";
	public static String Appoinment = "date-256";
	public static String Gender = "Sex";
	public static String MessageField = "Message";
        public static TestData td;
        
	public void startSingleTest() {

		driver.findElement(By.name(FirstName)).sendKeys(td.FirstName);
		driver.findElement(By.name(LastName)).sendKeys(td.LastName);
		driver.findElement(By.name(Email)).sendKeys(td.Email);
		driver.findElement(By.name(Mobile)).sendKeys(td.Mobile);
		driver.findElement(By.name(Appoinment)).sendKeys(td.AppointmentDate);
		driver.findElement(By.name(MessageField)).sendKeys(td.MessageField);

		Select S = new Select(driver.findElement(By.name(Gender)));
		S.selectByVisibleText(td.Gender);

		WebElement cityElement = ElementsFinder.findByMultipleParameters(driver, "input", "placeholder", "Your City",
				null);
		WebElement buttonElement = ElementsFinder.findByMultipleParameters(driver, "input", "value", "Submit", null);
		

		cityElement.sendKeys(td.CityName);
		buttonElement.submit();

	}

}
