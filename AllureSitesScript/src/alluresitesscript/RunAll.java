package alluresitesscript;

import gvjava.org.json.*;
import java.io.FileWriter;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;
//import org.testng.annotations.AfterTest;
//import org.testng.annotations.BeforeTest;
//import org.testng.annotations.Test;

public class RunAll {

    public static WebDriver driver = null;
    public static TestData td = null;
    public static int WaitTimeForTYPgSec = 120;
    public static JSONObject mainJSON = null;
    public static JSONArray urlArray = null;
    public static JSONObject urlArrayItem = null;

    public static void waitTillURLChanges(WebDriver driver, String currentURL) throws Exception {
        long timeMillis = System.currentTimeMillis();
        long timeSeconds = TimeUnit.MILLISECONDS.toSeconds(timeMillis);
        long errorReportTime = timeSeconds + WaitTimeForTYPgSec;

        while (driver.getCurrentUrl().equals(currentURL)) {
            timeMillis = System.currentTimeMillis();
            timeSeconds = TimeUnit.MILLISECONDS.toSeconds(timeMillis);

            if (timeSeconds > errorReportTime) {
                throw new FormNotSubmittedException("Error in submitted the form.");
            }
        }
    }

    public static void startTests(String reportPart) {

        int counter = 0;
        boolean successflag = true;
        String errorString = "";

        urlArray = new JSONArray();
        mainJSON = new JSONObject();

        while (URLCollection.next()) {
            successflag = true;
//            System.out.println(URLCollection.getURL());
//            System.out.println("Page Hit");
            
            driver.manage().timeouts().implicitlyWait(WaitTimeForTYPgSec, TimeUnit.SECONDS);
            driver.manage().timeouts().pageLoadTimeout(WaitTimeForTYPgSec, TimeUnit.SECONDS);

            try {
                URLCollection.getURL();
//                System.out.println("calling URL - " + URLCollection.getURL());
                driver.get(URLCollection.getURL());
            } catch (Exception Ex) {
                successflag = false;
            }
//            System.out.println("Page Loaded");

//            driver.manage().timeouts().implicitlyWait(600, TimeUnit.SECONDS);
//            driver.manage().timeouts().pageLoadTimeout(600, TimeUnit.SECONDS);
            switch (URLCollection.getCategory()) {
                case 1:
                    FormType1.driver = driver;
                    FormType1.td = td;
                    FormType1 form1 = new FormType1();
                    try {
                        form1.startSingleTest();
                    } catch (Exception ex) {
                        errorString = ex.toString();
                        successflag = false;
                    }

                    break;
                case 2:
                    FormType2.driver = driver;
                    FormType2.td = td;
                    FormType2 form2 = new FormType2();
                    try {
                        form2.startSingleTest();
                    } catch (Exception ex) {
                        errorString = ex.toString();
                        successflag = false;
                    }
                    break;
                case 3:
                    FormType3.driver = driver;
                    FormType3.td = td;
                    FormType3 form3 = new FormType3();
                    try {
                        form3.startSingleTest();
                    } catch (Exception ex) {
                        errorString = ex.toString();
                        successflag = false;
                    }
                    break;
                case 4:
                    FormType4.driver = driver;
                    FormType4.td = td;
                    FormType4 form4 = new FormType4();
                    try {
                        form4.startSingleTest();
                    } catch (Exception ex) {
                        errorString = ex.toString();
                        successflag = false;
                    }
                    break;
                case 5:
                    FormType5.driver = driver;
                    FormType5.td = td;
                    FormType5 form5 = new FormType5();
                    try {
//                        System.out.println("Cat 5 form module called");
                        form5.startSingleTest();
//                        System.out.println("Cat 5 form module Completed");
                    } catch (Exception ex) {
//                        System.out.println("Cat 5 form module ERROR");
                        errorString = ex.toString();
                        successflag = false;
                    }
                    break;
            }

            counter++;

            if (successflag) {

                try {
                    waitTillURLChanges(driver, URLCollection.getURL());
                    String submittedURL = driver.getCurrentUrl();
                    logStatus("-------------------------------------------------------------------------------");
                    logStatus(counter);
                    logStatus("Category:      " + URLCollection.getCategory());
                    logStatus("URL:           " + URLCollection.getURL());
                    logStatus("Submitted URL: " + submittedURL);
                    logStatus("-------------------------------------------------------------------------------");
                    printStatus(URLCollection.getURL(), "Passed", submittedURL);
                    try {
                        urlArrayItem = new JSONObject();
                        urlArrayItem.put("_id", URLCollection.getCategory());
                        urlArrayItem.put("SrNo", counter);
                        urlArrayItem.put("url", URLCollection.getURL());
                        urlArrayItem.put("ThankYouURL", submittedURL);
                        urlArrayItem.put("Status", "Passed");
                        urlArray.put(urlArrayItem);
                    } catch (Exception ex) {

                    }
                } catch (Exception ex) {
                    logStatus("-------------------------------------------------------------------------------");
                    logStatus(counter);
                    logStatus("Category:      " + URLCollection.getCategory());
                    logStatus("URL:           " + URLCollection.getURL());
                    logStatus("Error:         " + ex.toString());
                    logStatus("-------------------------------------------------------------------------------");
                    printStatus(URLCollection.getURL(), "Failed", "");

                    try {
                        urlArrayItem = new JSONObject();
                        urlArrayItem.put("_id", URLCollection.getCategory());
                        urlArrayItem.put("SrNo", counter);
                        urlArrayItem.put("url", URLCollection.getURL());
                        urlArrayItem.put("ThankYouURL", "");
                        urlArrayItem.put("Status", "Failed");
                        urlArray.put(urlArrayItem);
                    } catch (Exception ex2) {

                    }
                }

            } else {
                logStatus("-------------------------------------------------------------------------------");
                logStatus(counter);
                logStatus("Category:      " + URLCollection.getCategory());
                logStatus("URL:           " + URLCollection.getURL());
                logStatus("Error:         " + errorString);
                logStatus("-------------------------------------------------------------------------------");
                printStatus(URLCollection.getURL(), "Failed", "");

                try {
                    urlArrayItem = new JSONObject();
                    urlArrayItem.put("_id", URLCollection.getCategory());
                    urlArrayItem.put("SrNo", counter);
                    urlArrayItem.put("url", URLCollection.getURL());
                    urlArrayItem.put("ThankYouURL", "");
                    urlArrayItem.put("Status", "Failed");
                    urlArray.put(urlArrayItem);
                } catch (Exception ex2) {

                }
            }
        }
        try {
            mainJSON.put("report", urlArray);
            FileWriter fout = null;

            if (reportPart == null) {
                fout = new FileWriter(System.getProperty("user.dir") + "\\dataFiles\\latestReport.json", false);
            } else {
                fout = new FileWriter(reportPart, false);
            }

            fout.write(mainJSON.toString());
            fout.close();
//            System.out.println(mainJSON.toString());;
        } catch (Exception ex2) {

        }
        logStatus("Completed");
    }

    public static void logStatus(Object state) {
//        System.out.println(state);
    }

    public static void printStatus(Object URL, Object status, Object submittedURL) {
        System.out.println(URL + " : " + status);
    }

    public static void main(String[] args) {
//        args = new String[5];
//        args[0] = "G:\\AllureSite Testing\\java\\V4.0\\TestData.txt";
//        args[1] = "G:\\AllureSite Testing\\java\\V4.0\\URLList_aritro.txt";
//        args[2] = "G:\\AllureSite Testing\\java\\V4.0";
//        args[3] = "G:\\AllureSite Testing\\java\\V4.0\\testRP.json";
//        args[4] = "120";

        if (args.length == 5) {
            System.out.println("Testing Started");
            try {
                WaitTimeForTYPgSec = Integer.parseInt(args[4]);
            } catch (Exception ex) {
                WaitTimeForTYPgSec = 120;
            }
            td = new TestData(args[0]);
            URLCollection.setURLS(args[1]);
            URLCollection.initializeURLDataSet();

            System.setProperty("webdriver.chrome.driver", args[2] + "\\chromedriver.exe");
            driver = new ChromeDriver();
            driver.manage().window().maximize();
            startTests(args[3]);
            driver.close();
        } else {
            System.out.println("Cannot Start Testing");
//            td = new TestData(null);
//            URLCollection.setURLS(null);
//            URLCollection.initializeURLDataSet();
//            
//            System.setProperty("webdriver.chrome.driver", System.getProperty("user.dir") + "\\chromedriver.exe");
//            driver = new ChromeDriver();
//            driver.manage().window().maximize();
//            startTests(null);
        }
        System.out.println("Done");
    }

}
