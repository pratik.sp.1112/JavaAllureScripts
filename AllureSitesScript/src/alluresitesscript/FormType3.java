package alluresitesscript;

//import org.testng.annotations.Test;
//import org.testng.annotations.BeforeTest;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
//import org.testng.annotations.AfterTest;

public class FormType3 {
	public static WebDriver driver = null;
	// All name attribute
	public static String FullName = "your-name";
	public static String Email = "your-email";
	public static String Mobile = "phone";
	public static String MessageField = "your-message";
	public static String SubmitBtn = "Send"; // value Attribute with input tag
        public static TestData td;

	public void startSingleTest() {

		driver.findElement(By.name(FullName)).sendKeys(td.FullName);
		driver.findElement(By.name(Email)).sendKeys(td.Email);
		driver.findElement(By.name(Mobile)).sendKeys(td.Mobile);
		driver.findElement(By.name(MessageField)).sendKeys(td.MessageField);
		/*
		 * try { Thread.sleep(1000); } catch(Exception ex) {
		 * 
		 * }
		 */

		WebElement button = ElementsFinder.findByMultipleParameters(driver, "input", "value", SubmitBtn, null);
		while (!button.isEnabled()) {
		}
		button.click();
	}

}
