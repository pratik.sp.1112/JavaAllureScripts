package alluresitesscript;

import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.StringSelection;
import java.util.*;
import java.io.*;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

public class TestData {

    public String FirstName = "Ashis";
    public String LastName = "Rawat";
    public String FullName = FirstName + " " + LastName;
    public String Email = "ashish19rawat@gmail.com";
    public String Age = "35";
    public String Height = "170";
    public String Weight = "70";
    public String Mobile = "8879799396";
    public String AppointmentDate = "21 December, 2017";
    public String MessageField = "It's a test entry message.";
    public String CityName = "Mumbai";
    public String CountryName = "India";
    public String Procedure = "Hair Treatment";
    public String SkypeID = "skype_id";
    public String Gender = "Female";
    public String DOB_mmddyyy = "08211998";
    public String State = "Maharashtra";
    public String AMSRegistration = "Yes";
    public String UAMSID = "UAMSID field";
    public String PreferredDate1_mmddyyy = "01252018";
    public String PreferredDate2_mmddyyy = "01282018";
    public String PreferredTime1 = "12:00 PM";
    public String PreferredTime2 = "05:00 PM";

    public String Diabetes = "yes";
    public String BloodPressure = "no";
    public String Allergy = "yes";
    public String SleepApnea = "no";

    public String WhatsappCheck = "yes";
    public String ViberCheck = "no";
    public String SkypeCheck = "no";
    public String PhoneCheck = "yes";
    public String EmailCheck = "yes";
    public String MorningCheck = "yes";
    public String AfternoonCheck = "yes";
    public String EveningCheck = "yes";
    public String AnytimeCheck = "yes";
    public String OtherEstimatedTimeline = "Any";
    public String Path1 = "G:\\AllureSite Testing\\Node JS\\FormTestingNode\\NodeJS\\public\\img\\5.png";
    public String Path2 = "G:\\AllureSite Testing\\Node JS\\FormTestingNode\\NodeJS\\public\\img\\4.png";
    public String Path3 = "G:\\AllureSite Testing\\Node JS\\FormTestingNode\\NodeJS\\public\\img\\3.png";

    public String getCaptchaText(WebElement CaptchaSpan) {

        String rawCaptcha = CaptchaSpan.getText();
        rawCaptcha = rawCaptcha.replaceAll("\\s", "");

        return rawCaptcha;
    }

    public void waitTillListPopulates(Select Dropdown) {
        List<WebElement> temp = null;

        while (temp == null) {
            temp = Dropdown.getOptions();
        }

        while (temp.size() <= 1) {
            temp = Dropdown.getOptions();
        }
    }

    TestData(String path) {
        FileReader fin = null;
        try {
            if (path == null) {
                fin = new FileReader(System.getProperty("user.dir") + "\\dataFiles\\TestData.txt");
            } else {
                fin = new FileReader(path);
            }
        } catch (FileNotFoundException ex) {
            Logger.getLogger(URLCollection.class.getName()).log(Level.SEVERE, null, ex);
        }

        BufferedReader buf = null;
        try {
            buf = new BufferedReader(fin);

        } catch (Exception ex) {
            Logger.getLogger(URLCollection.class.getName()).log(Level.SEVERE, null, ex);
        }
        String line, key, value, datatype, format;

        try {
//            Set<String> d = new HashSet<String>();
            while ((line = buf.readLine()) != null) {
//                System.out.println(line);

                if (line.trim().length() <= 0) {
                    continue;
                }

                key = line.substring(0, line.indexOf('=')).trim();
                value = line.substring(line.indexOf('=') + 1, line.lastIndexOf('#')).trim();
                datatype = line.substring(line.lastIndexOf('#') + 1, line.lastIndexOf('$')).trim();
                format = line.substring(line.lastIndexOf('$') + 1).trim();

                if (format == null) {
                    format = "";
                }

//                System.out.println(key);
//                System.out.println(value);
//                System.out.println(datatype);
//                System.out.println(format);
//                System.out.println("---");
//                d.add(datatype);
//                setTextToClipboard(key);
                setData(key, value, datatype, format);
//                System.out.println(convertToSpecificFormat(value, datatype));
            }
//            System.out.println(d);
        } catch (IOException ex) {
            Logger.getLogger(URLCollection.class.getName()).log(Level.SEVERE, null, ex);
        }

        try {
            buf.close();
            fin.close();
        } catch (IOException ex) {
            Logger.getLogger(URLCollection.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void setData(String key, String value, String datatype, String format) {

        if (key.equals("First Name")) {
            FirstName = convertToSpecificFormat(value, datatype, format);
        } else if (key.equals("Email ID")) {
            Email = convertToSpecificFormat(value, datatype, format);
        } else if (key.equals("Age")) {
            Age = convertToSpecificFormat(value, datatype, format);
        } else if (key.equals("Height")) {
            Height = convertToSpecificFormat(value, datatype, format);
        } else if (key.equals("Last Name")) {
            LastName = convertToSpecificFormat(value, datatype, format);
        } else if (key.equals("Weight")) {
            Weight = convertToSpecificFormat(value, datatype, format);
        } else if (key.equals("Country Name")) {
            CountryName = convertToSpecificFormat(value, datatype, format);
        } else if (key.equals("State Name")) {
            State = convertToSpecificFormat(value, datatype, format);
        } else if (key.equals("Preferred Time 1")) {
            PreferredTime1 = convertToSpecificFormat(value, datatype, format);
        } else if (key.equals("Sleep Apnea Problem")) {
            SleepApnea = convertToSpecificFormat(value, datatype, format);
        } else if (key.equals("Contact Via Email")) {
            EmailCheck = convertToSpecificFormat(value, datatype, format);
        } else if (key.equals("Other Estimated Timeline")) {
            OtherEstimatedTimeline = convertToSpecificFormat(value, datatype, format);
        } else if (key.equals("Mobile No.")) {
            Mobile = convertToSpecificFormat(value, datatype, format);
        } else if (key.equals("Procedure")) {
            Procedure = convertToSpecificFormat(value, datatype, format);
        } else if (key.equals("AMS Registration")) {
            AMSRegistration = convertToSpecificFormat(value, datatype, format);
        } else if (key.equals("Preferred Time 2")) {
            PreferredTime2 = convertToSpecificFormat(value, datatype, format);
        } else if (key.equals("Contact Via WhatsApp")) {
            WhatsappCheck = convertToSpecificFormat(value, datatype, format);
        } else if (key.equals("Contact At Morning")) {
            MorningCheck = convertToSpecificFormat(value, datatype, format);
        } else if (key.equals("Appointment Date")) {
            AppointmentDate = convertToSpecificFormat(value, datatype, format);
        } else if (key.equals("Skype ID")) {
            SkypeID = convertToSpecificFormat(value, datatype, format);
        } else if (key.equals("UAMSID")) {
            UAMSID = convertToSpecificFormat(value, datatype, format);
        } else if (key.equals("Diabetes Problem")) {
            Diabetes = convertToSpecificFormat(value, datatype, format);
        } else if (key.equals("Contact Via Viber")) {
            ViberCheck = convertToSpecificFormat(value, datatype, format);
        } else if (key.equals("Contact At Afternoon")) {
            AfternoonCheck = convertToSpecificFormat(value, datatype, format);
        } else if (key.equals("City Name")) {
            CityName = convertToSpecificFormat(value, datatype, format);
        } else if (key.equals("Your Message")) {
            MessageField = convertToSpecificFormat(value, datatype, format);
        } else if (key.equals("Date Of Birth")) {
            DOB_mmddyyy = convertToSpecificFormat(value, datatype, format);
        } else if (key.equals("Gender")) {
            Gender = convertToSpecificFormat(value, datatype, format);
        } else if (key.equals("Preferred Date 2")) {
            PreferredDate2_mmddyyy = convertToSpecificFormat(value, datatype, format);
        } else if (key.equals("Preferred Date 1")) {
            PreferredDate1_mmddyyy = convertToSpecificFormat(value, datatype, format);
        } else if (key.equals("Allergy Problem")) {
            Allergy = convertToSpecificFormat(value, datatype, format);
        } else if (key.equals("Blood Pressure Problem")) {
            BloodPressure = convertToSpecificFormat(value, datatype, format);
        } else if (key.equals("Contact Via Phone")) {
            PhoneCheck = convertToSpecificFormat(value, datatype, format);
        } else if (key.equals("Contact Via Skype")) {
            SkypeCheck = convertToSpecificFormat(value, datatype, format);
        } else if (key.equals("Contact Anytime")) {
            AnytimeCheck = convertToSpecificFormat(value, datatype, format);
        } else if (key.equals("Contact At Evening")) {
            EveningCheck = convertToSpecificFormat(value, datatype, format);
        }
        FullName = FirstName + " " + LastName;
    }

    public String convertToSpecificFormat(String value, String datatype, String format) {
        if (datatype.equals("string") || datatype.equals("email") || datatype.equals("multilinestring") || datatype.equals("gender")) {
            return value;
        } else if (datatype.equals("number")) {
            return value;
        } else if (datatype.equals("yes-no")) {
            return value.toLowerCase();
        } else if (datatype.equals("date")) {
            try {
                Calendar c = javax.xml.bind.DatatypeConverter.parseDateTime(value);
                DateFormat dateFormat = new SimpleDateFormat(format);
                Date date = c.getTime();

//                System.out.println(format + " -- " + dateFormat.format(date));
                return dateFormat.format(date);
            } catch (Exception ex) {
                return value;
            }
        } else if (datatype.equals("time")) {

            try {
                SimpleDateFormat _24HourFotmat = new SimpleDateFormat("HH:mm");
                SimpleDateFormat DesiredFormat = new SimpleDateFormat(format);
                Date _24HourDt = _24HourFotmat.parse(value);

//                System.out.println(format + " -- " + DesiredFormat.format(_24HourDt));
                return DesiredFormat.format(_24HourDt);
            } catch (Exception e) {
                return value;
            }
        }
        return "";
    }

}
