package alluresitesscript;

//import org.testng.annotations.Test;
//import org.testng.annotations.BeforeTest;

import java.util.List;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;
//import org.testng.annotations.AfterTest;

public class FormType4 {
	public static WebDriver driver = null;
	// All name attribute
	public static String FullName = "name";
	public static String Email = "email";
	public static String SkypeID = "skype";
	public static String Mobile = "mobile";
	public static String Gender = "gender"; // DD
	public static String DOB = "dob"; // Calender
	public static String Country = "countries"; // DD
	public static String State = "State"; // DD
	public static String AMS = "registered"; // DD
	public static String UAMSID = "uamsid";
	public static String Procedure = "procedure";
	public static String PreferredDate1 = "PrefferedDate1"; // Calender
	public static String PreferredTime1 = "PreferredTime1";
	public static String PreferredDate2 = "PrefferedDate2"; // Calender
	public static String PreferredTime2 = "PreferredTime2";
	public static String MessageField = "your-message";
	public static String SubmitBtn = "Book Now"; // Value attribute with input tag
        public static TestData td;

	public void startSingleTest() {

		// driver.findElement(By.xpath("//*[@id=\"wpcf7-f2107-p1934-o1\"]/form/p[1]/span/input")).sendKeys(td.FullName);

		// ElementsFinder.findByMultipleParameters(driver, "input", "name", FullName,
		// null).sendKeys(td.FullName);;

		driver.findElement(By.name(FullName)).sendKeys(td.FullName);
		driver.findElement(By.name(Email)).sendKeys(td.Email);
		driver.findElement(By.name(SkypeID)).sendKeys(td.SkypeID);
		driver.findElement(By.name(Mobile)).sendKeys(td.Mobile);
		driver.findElement(By.name(DOB)).sendKeys(td.DOB_mmddyyy);
		driver.findElement(By.name(UAMSID)).sendKeys(td.UAMSID);
		driver.findElement(By.name(Procedure)).sendKeys(td.Procedure);
		driver.findElement(By.name(PreferredDate1)).sendKeys(td.PreferredDate1_mmddyyy);
		driver.findElement(By.name(PreferredTime1)).sendKeys(td.PreferredTime1);
		driver.findElement(By.name(PreferredDate2)).sendKeys(td.PreferredDate2_mmddyyy);
		driver.findElement(By.name(PreferredTime2)).sendKeys(td.PreferredTime2);
		driver.findElement(By.name(MessageField)).sendKeys(td.MessageField);

		Select GenderD, CountryD, StateD, AMSD;

		GenderD = new Select(driver.findElement(By.name(Gender)));
		td.waitTillListPopulates(GenderD);
		GenderD.selectByVisibleText(td.Gender);

		CountryD = new Select(driver.findElement(By.name(Country)));
		td.waitTillListPopulates(CountryD);
		CountryD.selectByVisibleText(td.CountryName);

		StateD = new Select(driver.findElement(By.name(State)));
		td.waitTillListPopulates(StateD);
		StateD.selectByVisibleText(td.State);

		AMSD = new Select(driver.findElement(By.name(AMS)));
		td.waitTillListPopulates(AMSD);
//                System.out.println(td.AMSRegistration);
                /////////////////
                
                int indexToSelect = -1;
                List<WebElement> temp = AMSD.getOptions();
                for(int i=0; i<temp.size(); i++){
                    if(temp.get(i).getText().toLowerCase().equals(td.AMSRegistration)){
                        indexToSelect = i;
                        break;
                    }
                }
                
                if(indexToSelect != -1){
                    AMSD.selectByIndex(indexToSelect);
                }
                
                ////////////////////////
//                AMSD.selectByVisibleText(td.AMSRegistration);

                
                
//                System.out.println("finding Button");
                WebElement submitBtn = ElementsFinder.findByMultipleParameters(driver, "input", "Value", SubmitBtn, null);
                
//                System.out.println(submitBtn.getText());
                
//                while(!submitBtn.isDisplayed()){}
//                while(!submitBtn.isEnabled()){}
                
		submitBtn.click();
	}

}
